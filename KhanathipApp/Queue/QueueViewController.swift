import UIKit
import SafariServices
import EventKit

class QueueViewController: UIViewController {// คลาสจัดกากรหน้า นัดหมาย

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var calendarButton: UIButton!
    @IBOutlet weak var appointmentImageIcon: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    
    // array data ที่ใช้ จำลองรายการนัดหมาย
    let data = [["duedate":"2023/10/20 12:31",
                "namecontact":"Cristiano Ronaldo",
                "date":"2023/10/23 13:00",
                 "link":"https://www.google.co.th"],
                ["duedate":"2023/10/20 13:31",
                 "namecontact":"Lionel Messi",
                 "date":"2023/10/23 14:00",
                 "link":"https://www.google.co.th"],
                ["duedate":"2023/10/20 14:31",
                 "namecontact":"Neymar",
                 "date":"2023/10/23 15:00",
                 "link":"https://www.google.co.th"],
                ["duedate":"2023/10/20 16:31",
                 "namecontact":"Antonela Roccuzzo",
                 "date":"2023/10/23 17:00",
                 "link":"https://www.google.co.th"],
                ["duedate":"2023/10/20 18:31",
                 "namecontact":"Kylian Mbappé",
                 "date":"2023/10/23 19:00",
                 "link":"https://www.google.co.th"],
                ["duedate":"2023/10/20 20:31",
                 "namecontact":"Haaland Norwegian",
                 "date":"2023/10/23 21:00",
                 "link":"https://www.google.co.th"],
    ]
        
    override func viewDidLoad() {
        super.viewDidLoad()

        self.initTableView();// เรียกเครื่องมือจาก Table View
        
    }
    
    @IBAction func calendarButtonAction(_ sender: UIButton) {
//
        
    }
    
}
//-MARK: Table View
extension QueueViewController: UITableViewDelegate, UITableViewDataSource{
    
    func initTableView(){// รวมการจัดการ การเรียกใข้เครื่องมือจาก Table View

        self.tableView.register(QueueTableViewCell.nib(), forCellReuseIdentifier: QueueTableViewCell.identifier)

        self.tableView.delegate = self
        self.tableView.dataSource = self

    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: QueueTableViewCell.identifier, for: indexPath) as! QueueTableViewCell
        
        cell.delegate = self
        cell.apply(data: self.data[indexPath.row])
        return cell;
       
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       
        return UITableView.automaticDimension
        
    }
}
//-MARK: Queue Table ViewCell Delegate
extension QueueViewController: QueueTableViewCellDelegate{ // จัดการฟังก์ขัน ที่มีการเรียกกลับมาจากการทำงานใน Table View Cell ของ Action ละ Cell
    func sendLink(link: String) {// แสดง Link
        if let url = URL(string: link) {
            let safariViewController = SFSafariViewController(url: url)
            present(safariViewController, animated: true, completion: nil)
        }

    }
    func copyName(text: String) { // คัดลองชื่อ
        self.copyText(text: text)
    
    }
    func copyLink(text: String) {// คัดลอง Link
        self.copyText(text: text)

    }
    func shareLink(text: String) { // แชร์ ข้อความ
        let textToShare = "Check out this cool app!"
        let urlToShare = URL(string: text)
        let itemsToShare: [Any] = [textToShare, urlToShare as Any].compactMap { $0 }
        let activityViewController = UIActivityViewController(activityItems: itemsToShare, applicationActivities: nil)
        present(activityViewController, animated: true, completion: nil)

    }

    func copyText(text: String){ // ฟังก์ขัน คัดลอง ข้อความ
        let pasteboard = UIPasteboard.general
        pasteboard.string = text
        
        let alert = UIAlertController(title: "Copied!", message: text, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
}
