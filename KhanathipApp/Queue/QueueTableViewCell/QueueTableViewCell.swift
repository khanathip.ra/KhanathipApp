import UIKit
import SafariServices

//protocol ที่ใช้ส่งข้อมูลของ Action ใน Cell
protocol QueueTableViewCellDelegate {
    func sendLink(link: String)
    func copyName(text: String)
    func copyLink(text: String)
    func shareLink(text: String)
}

class QueueTableViewCell: UITableViewCell {
    
    enum DelegateMode: String{// enum ในการแยกลักษณะการใช้งาน call Delegate
        case none = ""
        case send = "send"
        case copyName = "copyName"
        case copyLink = "copyLink"
        case shareLink = "shareLink"
    }
    
    static let identifier = "cellqueue"

    static func nib() -> UINib {// กำหนดชื่อไฟล์ nib ของให้กับการเรียก Cell
      return UINib(nibName: "QueueTableViewCell", bundle: nil)
    }
    
    var delegate: QueueTableViewCellDelegate? = nil

    @IBOutlet weak var containnerView: UIView!
    
    @IBOutlet weak var dueDateLabel: UILabel!
    @IBOutlet weak var nameContactLabel: UILabel!
    @IBOutlet weak var dateTimeLabel: UILabel!
    @IBOutlet weak var viewBookingButton: UIButton!
    @IBOutlet weak var copyNameButton: UIButton!
    @IBOutlet weak var copyLinkButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
    
    var dataDictionary: Dictionary<String, String> = [:]
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // กำหนดค่าเริ่มต้นให้หน้าจอ
        self.viewBookingButton.setTitle("view booking page", for: .normal)
        self.copyNameButton.setTitle("copy name", for: .normal)
        self.copyLinkButton.setTitle("copy link", for: .normal)
        self.shareButton.setTitle("share", for: .normal)
        
        self.copyNameButton.addTarget(self, action: #selector(copyText), for: .touchUpInside)
        self.copyLinkButton.addTarget(self, action: #selector(copyLink), for: .touchUpInside)
        self.shareButton.addTarget(self, action: #selector(shareLink), for: .touchUpInside)

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func apply(data: Dictionary<String, String>){// กำหนดค่าให้หน้าจอ หลังมีการเรียกใช้งาน
        self.containnerView.layer.cornerRadius = 16
        
        self.dataDictionary = data
        
        self.dueDateLabel.text = "due " + (data["duedate"] ?? "")
        self.nameContactLabel.text = data["namecontact"]
        self.dateTimeLabel.text = data["date"]

    }
    
//-MARK: Action
    
    @IBAction func viewBookingActionButton(_ sender: Any) {
        if let link = self.dataDictionary["link"]{
            self.callDelegate(delegateMode: .send, text: link)
        }

        print(self.dataDictionary)
    }
    
    @objc func copyText() {
        if let name = self.dataDictionary["namecontact"]{
            self.callDelegate(delegateMode: .copyName, text: name)
        }
    }
    @objc func copyLink() {
        if let link = self.dataDictionary["link"]{
            self.callDelegate(delegateMode: .copyLink, text: link)
        }
    }
    @objc func shareLink() {
        if let link = self.dataDictionary["link"]{
            self.callDelegate(delegateMode: .shareLink, text: link)
        }
    }
//-MARK: Function
    // การจัดการ ส่งข้อมูลผ่าน delegate โดยแยก delegate Mode
    func callDelegate(delegateMode: DelegateMode, text: String){
        
        if self.delegate != nil{
            let dataToBeSent = text
            switch delegateMode{
            case .none:
                break;
            case .send :
                self.delegate?.sendLink(link: dataToBeSent)
                break;
            case .copyName:
                self.delegate?.copyName(text: dataToBeSent)
                break;
            case .copyLink:
                self.delegate?.copyLink(text: dataToBeSent)
                break;
            case .shareLink:
                self.delegate?.shareLink(text: dataToBeSent)
                break;
            }
        }
    }
}
