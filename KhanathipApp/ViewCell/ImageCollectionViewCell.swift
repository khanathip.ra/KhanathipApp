//
//  ImageCollectionViewCell.swift
//  KhanathipApp
//
//  Created by khanathipMac on 22/10/2566 BE.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell {
    static let identifier = "cellImage"

    @IBOutlet weak var imageView: UIImageView!
    

    func apply() {
        //
    }
}
