import UIKit

class ViewController: UIViewController {// คลาสที่ใช้จัดการหน้าหลัก


    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var titleMenuLabel: UILabel!
    @IBOutlet weak var imageCollection: UICollectionView!
    @IBOutlet weak var imageScrollView: UIScrollView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var menuCollection: UICollectionView!
    
    let imageArray = ["preview-1","preview-2","preview-3"] // ชื่อรูปจาก Assets
    let items = ["Item 1", "Item 2", "Item 3", "Item 4", "Item 5", "Item 6", "Item 7", "Item 8", "Item 9"] // ชื่อเมนู
    let itemsPerRow = 3 //จำนวนเมนู ต่อ แถว
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initCollection() // เรียกการใช้งาน เครื่องมือ Collection
        self.imageScrollView.delegate = self
        
    }

}
//-MARK: Collection View 
extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    // ฟังก์ชันรวมการเรียกเครื่องมือ Collection
    func initCollection(){
        self.imageCollection.delegate = self
        self.imageCollection.dataSource = self
        self.menuCollection.delegate = self
        self.menuCollection.dataSource = self
    }

    // ฟังก์ชันจำนวน Collection
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        // if แยก Collection ใน view เดียวกัน เพื่อกำหนดจำนวน cell ที่ต้องการแสดง
        if collectionView == imageCollection{
            return imageArray.count

        }else if collectionView == menuCollection{
            return 6
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        // if แยก การเรียกใช้ cell จากคนละ file cell
        if collectionView == imageCollection{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellImage", for: indexPath) as! ImageCollectionViewCell

            let image = imageArray[indexPath.row]
            cell.imageView.image = UIImage(named: image)
            
            return cell

        }else if collectionView == menuCollection{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellmenu", for: indexPath) as! MenuCollectionViewCell

            cell.layer.cornerRadius = 16
            cell.titleLabel.text = items[indexPath.item]
            return cell
        }
        return UICollectionViewCell()
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        guard let collectionView = scrollView as? UICollectionView else {
            return
        }
        if collectionView == imageCollection{
            // การกำหนด pageControl ระหว่างการเลื่อนรูป
            let contentOffset = collectionView.contentOffset
            let center = CGPoint(x: collectionView.bounds.size.width / 2.0 + contentOffset.x,
                                 y: collectionView.bounds.size.height / 2.0 + contentOffset.y)

            if let indexPath = collectionView.indexPathForItem(at: center) {
                let cellIndex = indexPath.row
                self.pageControl.currentPage = Int(cellIndex)

            }
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == menuCollection{
            // การคำนวณหาขนาด width ของเมนูเพื่อ ใช้แสดงเมนูตามจำนวนที่กำนดต่อ แถว (itemsPerRow)
            let width = (menuCollection.bounds.width - 20) / CGFloat(itemsPerRow)
            let height = width
            return CGSize(width: width, height: height)
        }else{
            let width = (menuCollection.bounds.width + 16) / CGFloat(itemsPerRow)
            let height = 200.0
            return CGSize(width: imageCollection.bounds.width, height: height)
        }
    }

}
