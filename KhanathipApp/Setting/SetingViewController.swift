//
//  SetingViewController.swift
//  KhanathipApp
//
//  Created by khanathipMac on 22/10/2566 BE.
//

import UIKit

class SetingViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var discriptionLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.image.layer.cornerRadius = 20.0
        self.nameLabel.text = "Cristiano Ronaldo"
        self.discriptionLabel.text = "Discription Cristiano Ronaldo"
    }
 
}
